﻿using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using System;
using System.IO;

namespace GeneratePDF
{
    class Program
    {
        static void Main(string[] args)
        {

            PdfDocument pdfDocument = new PdfDocument(new PdfWriter(new FileStream("D:/testFilePDF/hello.pdf", FileMode.Create, FileAccess.Write)));
            Document document = new Document(pdfDocument);

            String line = "Hello! Welcome to iTextPdf";
            document.Add(new Paragraph(line));

            document.Close();
            byte[] user = System.Text.Encoding.ASCII.GetBytes("user");
            byte[] pass = System.Text.Encoding.ASCII.GetBytes("12345");
            PdfReader reader = new PdfReader("D:/testFilePDF/hello.pdf");
            WriterProperties props = new WriterProperties()
                    .SetStandardEncryption(user, pass, EncryptionConstants.ALLOW_PRINTING,
                            EncryptionConstants.ENCRYPTION_AES_128 | EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
            PdfWriter writer = new PdfWriter(new FileStream("D:/testFilePDF/hello22.pdf", FileMode.Create, FileAccess.Write), props);
            PdfDocument pdfDoc = new PdfDocument(reader, writer);
            pdfDoc.Close();
            Console.WriteLine("Awesome PDF just got created.");
        }
    }
}
